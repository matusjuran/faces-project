import numpy as np
np.random.seed(58) # for reproducibility
import tensorflow as tf
tf.random.set_seed(58)
from keras.utils import to_categorical
from matplotlib import pyplot as plt
from params import get_params
from utils import load_preprocess
from model import get_model

option = int(input())

params = get_params(option)
filters = params.filters
kernel_size = params.kernel_size
pool_size = params.pool_size
input_shape = params.input_shape
epochs = params.epochs

training_real, real_y = load_preprocess('./dataset/training_real/*', 0, input_shape, option)
training_fake, fake_y = load_preprocess('./dataset/training_fake/*', 1, input_shape, option)
training_set = np.concatenate((training_real, training_fake))
training_y = np.concatenate((real_y, fake_y))

cnn = get_model(filters, kernel_size, pool_size, input_shape)

cnn.compile(
  'adam',
  loss='binary_crossentropy',
  metrics=['accuracy'],
)

history = cnn.fit(
  training_set,
  to_categorical(training_y),
  epochs=epochs,
  validation_split=0.2,
  verbose=True
)

cnn.save_weights(f'trained_models/cnn{option}.h5')

training_score = cnn.evaluate(training_set, to_categorical(training_y))
print("\n\ntrain loss: {} | train acc: {}\n".format(training_score[0], training_score[1]))

plt.figure()
plt.plot(history.history['loss'], label='training loss')
plt.plot(history.history['val_loss'], label='validation loss')
plt.legend(loc='best')
plt.savefig(f'graphs/graph{option}-loss.png')

plt.figure()
plt.plot(history.history['accuracy'], label='train accuracy')
plt.plot(history.history['val_accuracy'], label='validation accuracy')
plt.legend(loc='best')

plt.savefig(f'graphs/graph{option}-accuracy.png')
