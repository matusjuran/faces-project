import numpy as np
import glob
from PIL import Image

def resize_image(im, dimensions):
	return im.resize(dimensions, Image.ANTIALIAS)

def grayscale_image(im):
	return im.convert('L')

def get_y(images, y_value):
	return np.array(len(images) * [y_value])

def load_preprocess(directory, y_value, dimensions, option):
	res = []
	for f in glob.glob(directory):
		im = Image.open(f)
		if option >= 1:
			im = resize_image(im, dimensions[:2])
		if option >= 2:
			im = grayscale_image(im)
		res.append(np.asarray(im))
	res = np.array(res)
	if option >= 2:
		res = np.expand_dims(res, axis=3)
	res = np.divide(res, 255)
	return (res, get_y(res, y_value))
