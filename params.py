class NoPreprocessing:
	filters = 3
	kernel_size = 3
	pool_size = 2
	input_shape = (600, 600, 3)
	epochs = 10

class Resized:
	filters = 3
	kernel_size = 3
	pool_size = 2
	input_shape = (100, 100, 3)
	epochs = 4

class ResizedGrayscaled:
	filters = 1
	kernel_size = 3
	pool_size = 2
	input_shape = (100, 100, 1)
	epochs = 10

def get_params(idx):
	params = [NoPreprocessing(), Resized(), ResizedGrayscaled()]
	return params[idx]
