import numpy as np
import tensorflow as tf
from keras import regularizers
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, BatchNormalization, Dropout, Activation, SpatialDropout2D, ReLU
from keras.models import Sequential
from keras.utils import to_categorical

def get_model(filters, kernel_size, pool_size, input_shape):
	return Sequential([
	  Conv2D(filters, kernel_size, input_shape=input_shape),
	  Conv2D(filters, kernel_size),
	  SpatialDropout2D(0.2),
	  BatchNormalization(),
	  MaxPooling2D(pool_size=pool_size),
	  Flatten(),
	  Dense(64, activation='relu'),
	  Dense(2, activation='softmax'),
	])
