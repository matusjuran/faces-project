import numpy as np
np.random.seed(58) # for reproducibility
import tensorflow as tf
tf.random.set_seed(58)
from keras.utils import to_categorical
from params import get_params
from utils import load_preprocess
from model import get_model

option = int(input())

params = get_params(option)
filters = params.filters
kernel_size = params.kernel_size
pool_size = params.pool_size
input_shape = params.input_shape

cnn = get_model(filters, kernel_size, pool_size, input_shape)

cnn.compile(
  'adam',
  loss='binary_crossentropy',
  metrics=['accuracy'],
)

testing_real, real_y = load_preprocess('./dataset/testing_real/*', 0, input_shape, option)
testing_fake, fake_y = load_preprocess('./dataset/testing_fake/*', 1, input_shape, option)
testing_set = np.concatenate((testing_real, testing_fake))
testing_y = np.concatenate((real_y, fake_y))

cnn.load_weights(f'trained_models/cnn{option}.h5')
testing_score = cnn.evaluate(testing_set, to_categorical(testing_y))
print("\n\ntest loss: {} | test acc: {}".format(testing_score[0], testing_score[1]))
